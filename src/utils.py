import secrets
import sqlite3
import tempfile
import time
from pathlib import Path
from typing import List
from math import log10, floor

import numpy as np
import pandas as pd


def save_to_csv(file_name: Path, df: pd.DataFrame) -> None:
    """Save the processed dataframe to a comma-separated value (csv) file
    If a dataframe is not specified, 'processed_data' is used
    """
    df.to_csv(file_name)


def save_to_pickle(file_name: Path, df: pd.DataFrame, protocol: int = 4) -> None:
    """Save a dataframe to a python pickle file
    If a dataframe is not specified, 'processed_data' is used
    """
    df.to_pickle(file_name, protocol=protocol)


def save_to_sqlite(file_name: Path, df: pd.DataFrame, table_name: str) -> None:
    """Save the processed dataframe to an sqlite database
    If a dataframe is not specified, 'processed_data' is used
    """
    conn = sqlite3.connect(file_name)
    df.to_sql(name=table_name, con=conn)
    conn.close()


def timestamp(fmt: str = "%Y%m%dT%H%M%S") -> str:
    """Return a timestamp based on the current local time
    """
    timestamp = time.strftime(fmt)
    return timestamp


def to_list(val, convert_None: bool = False) -> List:
    """Convert a value to a list.
    """
    from pathlib import Path

    if isinstance(val, Path):
        val = str(val)
    if isinstance(val, (str, int, float)):
        plist = [val]
    elif val is None and convert_None:
        plist = []
    else:
        try:
            plist = list(val)
        except Exception:
            raise ValueError("Error: Input must be convertible to a list")
    return plist


def get_random_filename(
    nbytes: int = 16, suffix: str = ".tmp", prefix: str = ""
) -> Path:
    """Create a random filename in a temporary directory

    The python `tempfile` module routines don't meet this need
    """
    tdir = Path(tempfile.mkdtemp())
    fname = prefix + "_" + secrets.token_hex(nbytes=nbytes) + suffix
    fpath = tdir / fname
    return fpath


def clean_dataframe(
    df: pd.DataFrame,
    remove_nan_rows: bool = False,
    remove_nan_cols: bool = False,
    replace_infs: bool = True,
) -> pd.DataFrame:
    """Perform various cleaning tasks on a dataframe, generally in preparation
    for input to a model
    """
    if replace_infs:
        df = df.replace([np.inf, -np.inf], np.nan)
    if remove_nan_rows:
        df = df.dropna(axis=0)
    if remove_nan_cols:
        df = df.dropna(axis=1)
    return df


def sample_from_array(array: np.ndarray, num_samples: int) -> np.ndarray:
    """Get a pseudo random sample of rows from an array"""
    indices = np.random.choice(array.shape[0], num_samples, replace=False)
    sampled_array = array[indices]
    return sampled_array


def to_1Darray(data):
    """Convert a datastructure to a 1D array
    Some of the ML methods complain if the data aren't formatted in this manner
    """
    if isinstance(data, pd.Series):
        data = data.to_numpy()
    if len(data.shape) == 1:
        data = data.reshape(-1, 1)
    if (len(data.shape) == 2) and (data.shape[1] == 1):
        data = np.ravel(data)
    return data


def round_sig(x, sig_figs=5):
    """Round a number to a specified number of significant figures
    """
    inner = log10(abs(float(x)))
    val = round(x, sig_figs - int(floor(inner)) - 1)
    return val
