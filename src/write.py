from pathlib import Path
from collections import defaultdict

import pandas as pd
import openpyxl
import openpyxl.cell._writer  # this is needed so that pyinstaller (v6.6.0) behaves properly
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.styles import Font, Alignment, colors
from openpyxl.utils import get_column_letter
from openpyxl.worksheet.hyperlink import Hyperlink

import config


def save_kc_data_to_excel(anl, cas_number: str, save_to: Path, template: Path = None):
    """Write out smmary data to an excel xlsx file

    Note that the variable naming is coupled to that in `summarize_kc_data` and also
    `get_kc_summary_data` in the `iarc_toxcast_viewer` module
    """

    hlinks = defaultdict(list)

    data_font = Font(name="Arial", size=11)

    if template is None:
        template = config.HTS_KC_OUTPUT_TEMPLATE

    workbook = openpyxl.load_workbook(template)
    statistics_sheet = workbook["statistics"]
    most_relevant_sheet = workbook["most_relevant"]
    full_output_sheet = workbook["full_output"]
    assays_sheet = workbook["active_assay_details"]

    details_data, master_col_map = anl.clean_kc_data_for_presentation(cas_number)

    # =====================================================================
    # fill the statistics sheet
    # =====================================================================
    summary_data = anl.summarize_kc_data(cas_number)

    statistics_sheet_locs = {
        "chemical_name": "B3",
        "cas_number": "B4",
        "kc1_active_no_flags": "B8",
        "kc1_active_w_flags": "C8",
        "kc1_inactive": "D8",
        "kc1_total": "E8",
        "kc2_active_no_flags": "B9",
        "kc2_active_w_flags": "C9",
        "kc2_inactive": "D9",
        "kc2_total": "E9",
        "kc3_active_no_flags": "B10",
        "kc3_active_w_flags": "C10",
        "kc3_inactive": "D10",
        "kc3_total": "E10",
        "kc4_active_no_flags": "B11",
        "kc4_active_w_flags": "C11",
        "kc4_inactive": "D11",
        "kc4_total": "E11",
        "kc5_active_no_flags": "B12",
        "kc5_active_w_flags": "C12",
        "kc5_inactive": "D12",
        "kc5_total": "E12",
        "kc6_active_no_flags": "B13",
        "kc6_active_w_flags": "C13",
        "kc6_inactive": "D13",
        "kc6_total": "E13",
        "kc7_active_no_flags": "B14",
        "kc7_active_w_flags": "C14",
        "kc7_inactive": "D14",
        "kc7_total": "E14",
        "kc8_active_no_flags": "B15",
        "kc8_active_w_flags": "C15",
        "kc8_inactive": "D15",
        "kc8_total": "E15",
        "kc9_active_no_flags": "B16",
        "kc9_active_w_flags": "C16",
        "kc9_inactive": "D16",
        "kc9_total": "E16",
        "kc10_active_no_flags": "B17",
        "kc10_active_w_flags": "C17",
        "kc10_inactive": "D17",
        "kc10_total": "E17",
    }

    for name, loc in statistics_sheet_locs.items():
        statistics_sheet[loc] = summary_data[name]

    # =====================================================================
    # fill the most relevant (active without flags) sheet
    # =====================================================================
    start_row = 4
    cols = [
        "KEYCHAR_NAME",
        "ASSAY_NAME",
        "ORGANISM",
        "TISSUE",
        "CELL_SHORT_NAME",
        "AC50",
        "ASSAY_DESCRIPTION",
    ]
    relevant_col_map = dict(zip(cols, range(1, len(cols) + 1)))

    # select the actives with no flags
    actives_filter = (details_data["FLAGS"] == "") & (
        details_data["ACTIVE"] == config.ACTIVE_SYMBOL
    )

    active_assays = details_data[actives_filter].copy()

    # we need to coerce the type to make it comparable to that in our other dataframe
    active_assays["ASSAY_ID"] = active_assays["ASSAY_ID"].astype(int)
    active_assay_ids = active_assays["ASSAY_ID"]

    # now pull the active assays out of the entire assay info database
    assay_info = anl._get_assay_info_data()
    active_assay_info = assay_info[assay_info["ASSAY_ID"].isin(active_assay_ids)].copy()
    active_assay_info["ASSAY_ID"] = active_assay_info["ASSAY_ID"].astype(int)

    # create a dataframe combining the kcs and assay details
    actives = pd.merge(active_assays, active_assay_info, on=["ASSAY_ID", "ASSAY_NAME"])

    actives = actives[cols]

    rows = dataframe_to_rows(actives, index=False, header=False)

    c_name_to_format = ["AC50"]
    c_idx_to_format = [relevant_col_map[c] for c in c_name_to_format]

    for r_idx, row in enumerate(rows, start_row):
        for c_idx, value in enumerate(row, 1):
            most_relevant_sheet.cell(row=r_idx, column=c_idx, value=value)
            most_relevant_sheet.cell(row=r_idx, column=c_idx).font = data_font
            if c_idx in c_idx_to_format:
                most_relevant_sheet.cell(row=r_idx, column=c_idx).alignment = Alignment(
                    horizontal="center"
                )

    # =====================================================================
    # fill the full_output sheet
    # =====================================================================
    start_row = 4

    rows = dataframe_to_rows(details_data, index=False, header=False)

    c_name_to_format = ["ASSAY_ID", "ACTIVE", "AC50"]
    c_idx_to_format = [master_col_map[c] for c in c_name_to_format]

    for r_idx, row in enumerate(rows, start_row):
        for c_idx, value in enumerate(row, 1):
            full_output_sheet.cell(row=r_idx, column=c_idx, value=value)
            full_output_sheet.cell(row=r_idx, column=c_idx).font = data_font
            if c_idx == master_col_map["ASSAY_ID"]:
                c_id = _xlref(r_idx, c_idx, zero_indexed=False)
                hlinks[str(value)].append(c_id)
            if c_idx in c_idx_to_format:
                full_output_sheet.cell(row=r_idx, column=c_idx).alignment = Alignment(
                    horizontal="center"
                )

    # =====================================================================
    # fill the assay sheet
    # =====================================================================
    start_row = 4

    cols = [
        "ASSAY_ID",
        "ASSAY_NAME",
        "ORGANISM",
        "TISSUE",
        "CELL_FORMAT",
        "CELL_SHORT_NAME",
        "ASSAY_DESCRIPTION",
    ]

    assay_col_map = dict(zip(cols, range(1, len(cols) + 1)))

    c_name_to_format = ["ASSAY_ID"]
    c_idx_to_format = [assay_col_map[c] for c in c_name_to_format]

    # focus on the active assays
    active_assays = details_data[details_data["ACTIVE"] == config.ACTIVE_SYMBOL]

    # we need to coerce the type to make it comparable to that in our other dataframe
    active_assay_ids = active_assays["ASSAY_ID"].astype("int")

    # now pull the active assays out of the entire assay info database
    assay_info = anl._get_assay_info_data()

    active_assay_info = assay_info[assay_info["ASSAY_ID"].isin(active_assay_ids)]

    rows = dataframe_to_rows(active_assay_info, index=False, header=False)

    for r_idx, row in enumerate(rows, start_row):
        for c_idx, value in enumerate(row, 1):
            assays_sheet.cell(row=r_idx, column=c_idx, value=value)
            assays_sheet.cell(row=r_idx, column=c_idx).font = data_font
            if c_idx == assay_col_map["ASSAY_ID"]:
                c_id = _xlref(r_idx, c_idx, zero_indexed=False)
                hlinks[str(value)].append(c_id)
            if c_idx in c_idx_to_format:
                assays_sheet.cell(row=r_idx, column=c_idx).alignment = Alignment(
                    horizontal="center"
                )

    # =====================================================================
    # use the information above to create some hyperlinks
    # =====================================================================
    # we collected references to items that may not match on both the target
    # and destination worksheets for the hyperlink.
    # If the length of the values list is 2, then we have a match.
    hlinks = {k: v for k, v in hlinks.items() if len(v) == 2}

    for assay_id, (at_cell, cell_ref) in hlinks.items():
        _create_hyperlink(
            full_output_sheet,
            at_cell,
            assays_sheet,
            cell_ref=cell_ref,
            display_name=str(assay_id),
        )

    # =====================================================================
    # make final format changes and write the results
    # =====================================================================
    for ws in workbook.worksheets:
        _adjust_column_width(ws)

    workbook.save(save_to)


def _xlref(row, column, zero_indexed=True):
    if zero_indexed:
        row += 1
        column += 1
    return get_column_letter(column) + str(row)


def _create_hyperlink(ws, at_cell, sheet_name, cell_ref, display_name):
    ws[at_cell].hyperlink = f"#{sheet_name.title}!{cell_ref}"
    ws[at_cell].value = display_name
    ws[at_cell].font = Font(u="single", color=colors.BLUE, name="Arial")


def _adjust_column_width(ws):
    """
    Adjust the width of spreadsheet columns for use in the context of openpyxl

    Taken from https://stackoverflow.com/a/68290263
    """

    def is_merged_horizontally(cell):
        """
        Checks if cell is merged horizontally with an another cell
        """
        cell_coor = cell.coordinate
        if cell_coor not in ws.merged_cells:
            return False
        for rng in ws.merged_cells.ranges:
            if cell_coor in rng and len(list(rng.cols)) > 1:
                return True
        return False

    for col_number, col in enumerate(ws.columns, start=1):
        col_letter = get_column_letter(col_number)

        max_length = max(
            len(str(cell.value or ""))
            for cell in col
            if not is_merged_horizontally(cell)
        )
        adjusted_width = (max_length + 2) * 0.95
        ws.column_dimensions[col_letter].width = adjusted_width
