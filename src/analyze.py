from collections import Counter, defaultdict
from pathlib import Path
from sqlite3.dbapi2 import DatabaseError
from typing import List, Mapping, Optional, Tuple

from pandas.core.frame import DataFrame

import _pickle
import pandas as pd

import config
import utils


class Analyzer:
    """This class manages a number of operations dealing with a ToxCast database
    create using the 'create_db' function in the 'process' module
    """

    def __init__(self, db_file: Path):
        self._load_db(db_file)

    # -----------------------------------------------------------------------------
    # Public methods
    # -----------------------------------------------------------------------------
    def query_data(
        self,
        assay_names: Optional[list] = None,
        cas_numbers: Optional[list] = None,
        save_to: Optional[Path] = None,
    ) -> pd.DataFrame:
        """Find all records with a certain assay_name (or names) and/or CASRN (or numbers)

        Returns a dataframe
        """
        assay_names = utils.to_list(assay_names, convert_None=True)
        cas_numbers = utils.to_list(cas_numbers, convert_None=True)
        self._verify_assay_and_cas(assay_names=assay_names, cas_numbers=cas_numbers)
        df = self.processed_data
        if assay_names and not cas_numbers:
            filter = df["ASSAY_NAME"].isin(assay_names)
        if not assay_names and cas_numbers:
            filter = df["CASRN"].isin(cas_numbers)
        if assay_names and cas_numbers:
            filter = (df["ASSAY_NAME"].isin(assay_names)) & (
                df["CASRN"].isin(cas_numbers)
            )
        results_df = df[filter]
        if save_to is not None:
            utils.save_to_csv(save_to, results_df)
        return results_df

    def get_toxcast_data_for_keychar(
        self,
        keychar_num: int,
        mapping_file: Optional[Path] = None,
        save_to: Optional[Path] = None,
    ) -> pd.DataFrame:
        """Return a dataframe with results corresponding to all of the assays related
        to a given key characteristic
        """
        mapping = self._keychar_to_toxcast_mapping(mapping_file=mapping_file)
        if keychar_num not in mapping:
            raise ValueError(
                f"Key characteristic '{keychar_num}' has not been mapped to ToxCast assays."
            )
        assays = mapping[keychar_num]
        df = self.query_data(assay_names=assays, save_to=save_to)
        return df

    def get_toxcast_data_for_all_keychars(
        self,
        cas_number: str,
        mapping_file: Optional[Path] = None,
        save_to: Optional[Path] = None,
    ) -> pd.DataFrame:
        """Given a CAS number, return a dataframe with results corresponding to all
        of the assays related to a given key characteristic.
        """
        kc_num_to_name = config.KC_NUM_TO_NAME
        cas_numbers = utils.to_list(cas_number)
        df_all = []
        mapping = self._keychar_to_toxcast_mapping(mapping_file=mapping_file)
        for kc_num, assays in mapping.items():
            df_kc = self.query_data(
                cas_numbers=cas_numbers, assay_names=assays, save_to=save_to
            )
            kc_name = kc_num_to_name.get(kc_num, config.KC_MISSING_SYMBOL)
            df_kc.insert(0, "KEYCHAR_NUM", kc_num)
            df_kc.insert(1, "KEYCHAR_NAME", kc_name)
            df_all.append(df_kc)
        df = pd.concat(df_all)
        df_sorted = df.sort_values(by=["KEYCHAR_NUM"])
        return df_sorted

    def filter_assay_results(
        self,
        df: pd.DataFrame,
        keychar_num: Optional[int] = None,
        hit_call: Optional[int] = None,
        has_flags: Optional[bool] = None,
    ) -> pd.DataFrame:
        """Filter on relevant criteria related to assay results
        """

        if keychar_num is None:
            keychar_filter = True
        else:
            keychar_filter = df["KEYCHAR_NUM"] == keychar_num

        iflags = df["FLAGS"].map(len) > 0
        flags_filter = iflags == has_flags
        hit_call_filter = df["HIT_CALL"] == hit_call

        if (has_flags is not None) and (hit_call is not None):
            filters = keychar_filter & flags_filter & hit_call_filter
        elif has_flags is not None:
            filters = keychar_filter & flags_filter
        elif hit_call is not None:
            filters = keychar_filter & hit_call_filter
        else:
            filters = keychar_filter

        df_new = df[filters]

        return df_new

    def clean_kc_data_for_presentation(self, cas_number: str):
        """
        """
        data = self.get_toxcast_data_for_all_keychars(cas_number)

        data.replace(to_replace="nan", value="--", inplace=True)
        data["AC50"] = data["AC50"].map(self._format_for_presentation)

        hit_call_to_activity = {0: config.INACTIVE_SYMBOL, 1: config.ACTIVE_SYMBOL}
        data["ACTIVE"] = data["HIT_CALL"].map(hit_call_to_activity)

        cols = [
            "KEYCHAR_NAME",
            "ASSAY_ID",
            "ASSAY_NAME",
            "ACTIVE",
            "AC50",
            "FLAGS",
        ]

        col_map = dict(zip(cols, range(1, len(cols) + 1)))

        details_data = data[cols]

        # drop rows where the KC is a missing value
        details_data = details_data[
            details_data["KEYCHAR_NAME"] != config.KC_MISSING_SYMBOL
        ]

        # replace nans for display
        details_data.fillna(config.NAN_SYMBOL, inplace=True)

        return details_data, col_map

    def summarize_kc_data(self, cas_number: str):
        """Get the total number of assays corresponding to each key characteristic and
        other criteria

        Note that the variable naming is coupled to that in `summarize_kc_data` and also
        `get_kc_summary_data` in the `iarc_toxcast_viewer` module
        """
        results = {}

        df = self.get_toxcast_data_for_all_keychars(cas_number)

        results["cas_number"] = cas_number
        results["chemical_name"] = self.cas_to_name_map[cas_number]

        kc_nums = range(1, 11)
        for kc in kc_nums:
            results[f"kc{kc}_active_w_flags"] = len(
                self.filter_assay_results(df, kc, hit_call=1, has_flags=True)
            )
            results[f"kc{kc}_active_no_flags"] = len(
                self.filter_assay_results(df, kc, hit_call=1, has_flags=False)
            )
            results[f"kc{kc}_inactive"] = len(
                self.filter_assay_results(df, kc, hit_call=0)
            )
            results[f"kc{kc}_total"] = len(self.filter_assay_results(df, kc))
        return results

    def get_inconsistent_data(self, df: pd.DataFrame = None) -> List:
        """Get a list of data that have inconsistent hit calls, given the same
        assay and cas number.
        """
        if df is None:
            df = self.processed_data
        # first, gather a list of replicates
        assay_names = df["ASSAY_NAME"].to_numpy()
        cas_numbers = df["CASRN"].to_numpy()
        hit_calls = df["HIT_CALL"].to_numpy()
        tups = zip(assay_names, cas_numbers, hit_calls)
        # For now, we will count matching (assay, cas, hit_call) tuples as being identical
        # even though they will be different assay runs and have different data and notes (FLAGS)
        # if we use a set data structure, we'll eliminate the replicates
        s_tups = set(tups)
        # eliminate the hit_calls, so we can count replicate (assay,cas) tuples
        pairs = [(assay, cas) for assay, cas, _ in s_tups]
        c = Counter(pairs)
        # create a count of pairs that represent the same (assay, cas) tuple,
        # but different hit_calls
        # for now, there isn't a need for using the count of replicates, so just
        # extract the dictionary keys
        replicates = list({k: cnt for k, cnt in c.items() if cnt > 1}.keys())
        return replicates

    def to_csv(self, fname: Path, df: pd.DataFrame) -> None:
        utils.save_to_csv(fname, df)

    def to_pickle(self, fname: Path, df: pd.DataFrame) -> None:
        utils.save_to_pickle(fname, df)

    def to_sqlite(self, fname: Path, df: pd.DataFrame) -> None:
        utils.save_to_sqlite(fname, df)

    # -------------------------------------------------------------------------
    # Private helper methods
    # -------------------------------------------------------------------------
    def _load_db(self, db_file: Path) -> None:
        """Load a saved db file into a dataframe

        As of now, this only works for pickled and sqlite files.
        """
        df = self._load_pickle_db(db_file)
        if df is None:
            df = self._load_sqlite_db(db_file)
        if df is not None:
            self.processed_data = df
            (
                self.assay_names,
                self.cas_numbers,
                self.preferred_names,
                self.smiles,
                self.cas_to_name_map,
                self.cas_to_smiles_map,
            ) = self._extract_info(df)
        else:
            raise TypeError(
                f"Cannot load file {db_file}. It must be a pickled dataframe or an sqlite file convertible to a dataframe"
            )

    def _get_assay_info_data(self, db_file: Optional[Path] = None) -> pd.DataFrame:
        """Return information about the ToxCast assays
        """
        if db_file is None:
            db_file = config.DEFAULT_ASSAY_INFO_SOURCE
        assay_info = self._load_pickle_db(db_file)
        return assay_info

    def _extract_info(self, df: pd.DataFrame) -> Tuple:
        """Extract certain columns from the dataframe
        """
        assay_names = sorted(df["ASSAY_NAME"].unique())
        df_cs = df.drop_duplicates(subset=["CASRN", "SMILES", "PREFERRED_NAME"])
        cas_numbers = df_cs["CASRN"].to_numpy()
        smiles = df_cs["SMILES"].to_numpy()
        preferred_names = df_cs["PREFERRED_NAME"].to_numpy()
        # keep the CASRN and PREFERRED_NAME in the same order
        tups = list(zip(cas_numbers, preferred_names))
        cas_to_name = dict(tups)
        # keep the CASRN and SMILES in the same order
        tups = list(zip(cas_numbers, smiles))
        cas_to_smiles = dict(tups)
        return (
            assay_names,
            cas_numbers,
            preferred_names,
            smiles,
            cas_to_name,
            cas_to_smiles,
        )

    def _format_for_presentation(self, x, err_val="", sig_figs=4):
        """Change the format of number for presentation
        """
        try:
            fval = float(x)
            val = float((f"{fval:.{sig_figs}g}"))
        except ValueError:
            val = err_val
        return val

    def _load_pickle_db(self, db_file: Path) -> pd.DataFrame:
        """Load a database that was saved in pickled format
        """
        df = None
        try:
            df = pd.read_pickle(db_file)
        except _pickle.UnpicklingError:
            pass
        return df

    def _verify_assay_and_cas(
        self, assay_names: Optional[list] = None, cas_numbers: Optional[list] = None,
    ) -> None:
        """Check that the arguments are valid

        Raises an exception for an invalid argument
        Returns None
        """
        if not assay_names and not cas_numbers:
            raise ValueError(
                "A value must be provided for either or both of the arguments."
            )
        if assay_names:
            for assay_name in assay_names:
                if assay_name not in self.assay_names:
                    raise ValueError(
                        f"The assay name '{assay_name}' was not found in the database."
                    )
        if cas_numbers:
            for cas in cas_numbers:
                if cas not in self.cas_numbers:
                    raise ValueError(
                        f"The CAS number '{cas}' was not found in the database."
                    )

    def _keychar_to_toxcast_mapping(
        self, mapping_file: Optional[Path] = None
    ) -> Mapping[int, list]:
        """Create a dictionary that maps key characteristic number to toxcast assay names

        The mapping file should be updated when there are changes
        """
        if mapping_file is None:
            mapping_file = config.KEYCHAR_HTS_MAPPING

        mapping = defaultdict(list)

        df = pd.read_csv(mapping_file, comment="#")

        for tup in df.itertuples():
            mapping[int(tup.KEYCHAR_NUMBER)].append(tup.ASSAY_NAME)

        return mapping
