import matplotlib.colors as mcolors
import numpy as np
import pandas as pd
import PySimpleGUI as sg
from matplotlib import pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from PySimpleGUI.PySimpleGUI import TabGroup

import analyze
import write
import config

# -------------------------------------------------------------------------
# Items shared throughout the module
# -------------------------------------------------------------------------
details_table_headers = [
    "Key characteristic",
    "Assay ID",
    "Assay name",
    "Active",
    "AC50 (mM)",
    "Flags",
]
summary_table_headers = [
    "Key characteristic",
    "Active: without flags",
    "Active: with flags",
    "Inactive",
    "Total",
]

plot_annotation_text = (
    "The height of the bars does not account for the relative\n"
    + "biological relevance of assays for the associated KC.\n"
    + "All active assays are considered to be of the same significance."
)


# -------------------------------------------------------------------------
# Functions for widget layout and data processing
# -------------------------------------------------------------------------
def init_db():
    db_datafile = config.DEFAULT_DATA_SOURCE
    anl = analyze.Analyzer(db_datafile)
    return anl


def get_chemicals_dict(anl):
    """
    Returns dictionary of chemicals
    Key is the chemical_name (cas_number)
    Value is the cas_number
    """

    preferred_names = anl.preferred_names
    cas_numbers = anl.cas_numbers
    idents = sorted(zip(preferred_names, cas_numbers))
    chemicals_dict = {}
    for pname, casn in idents:
        chemicals_dict[f"{pname} ({casn})"] = casn
    return chemicals_dict


def get_chemicals_list(anl):
    """
    Returns list of chemical_names to display
    """
    return sorted(list(get_chemicals_dict(anl).keys()))


def filter_details_data(df, keychar_num=None, active=None, has_flags=None):
    """
    Filter the details data based on several criteria
    """

    # create arrays of True instead of just True
    # to prevent errors in the case where our final filter would be just True or False
    # instead of something with the proper shape

    if keychar_num is None:
        keychar_filter = True
    else:
        keychar_name = config.KC_NUM_TO_NAME[keychar_num]
        keychar_filter = df["KEYCHAR_NAME"] == keychar_name

    if active is None:
        active_filter = True
    elif active == True:
        active_filter = df["ACTIVE"] == config.ACTIVE_SYMBOL
    elif active == False:
        active_filter = df["ACTIVE"] == config.INACTIVE_SYMBOL

    if has_flags is None:
        flags_filter = True
    else:
        iflags = df["FLAGS"].map(len) > 0
        flags_filter = iflags == has_flags

    filters = keychar_filter & active_filter & flags_filter

    # there is a case where each filter could be a simple boolean, leading to
    # filters being a boolean. This means that we will try to index
    # our dataframe on a single value, which will fail.
    # In these cases, return the original dataframe unchanged.
    try:
        filtered_df = df[filters]
    except KeyError:
        filtered_df = df

    return filtered_df


def create_kc_table(data, headers, key="-Table-"):
    """ """
    kc_table = sg.Table(
        values=data.tolist(),
        headings=headers,
        display_row_numbers=False,
        auto_size_columns=True,
        num_rows=min(25, len(data)),
        enable_events=True,
        font=config.GUI_NORMAL_FONT,
        justification="center",
        select_mode=sg.TABLE_SELECT_MODE_NONE,
        k=key,
    )
    return kc_table


def create_chem_listbox(anl):
    chemicals_list = get_chemicals_list(anl)
    chem_listbox = sg.Listbox(
        values=chemicals_list,
        select_mode=sg.SELECT_MODE_SINGLE,
        size=(50, 20),
        font=config.GUI_NORMAL_FONT,
        bind_return_key=True,
        key="-CHEMICALS LIST-",
    )
    return chem_listbox


def create_plot_canvas():
    plt_canvas = sg.Canvas(pad=10, key="-FIGURE CANVAS-")
    return plt_canvas


def create_chem_listbox_controls():
    controls = (
        sg.Text(
            "Filter (F1):",
            font=config.GUI_NORMAL_FONT,
        ),
        sg.Input(
            size=(25, 1),
            focus=True,
            enable_events=True,
            key="-CHEMICALS FILTER BOX-",
        ),
        sg.Text(size=(15, 1), font=config.GUI_NORMAL_FONT, k="-NUMBER REMAINING-"),
        sg.Button("Clear filter", font=config.GUI_NORMAL_FONT, k="-CLEAR-"),
    )
    return controls


def create_filter_choice_elements():
    filter_elems = [
        sg.Text(
            "Key characteristic:",
            justification="right",
            font=config.GUI_NORMAL_FONT,
        ),
        sg.Combo(
            ["All"] + list(config.KC_NUM_TO_NAME.values()),
            size=(40, 1),
            k="-KEYCHAR CHOICE-",
            enable_events=True,
            readonly=True,
            default_value="All",
            font=config.GUI_NORMAL_FONT,
        ),
        sg.Text("Assay activity:", justification="right", font=config.GUI_NORMAL_FONT),
        sg.Combo(
            ["All", "Active", "Inactive"],
            size=(10, 1),
            k="-ACTIVITY CHOICE-",
            enable_events=True,
            readonly=True,
            default_value="All",
            font=config.GUI_NORMAL_FONT,
        ),
        sg.Text("Flags:", justification="right", font=config.GUI_NORMAL_FONT),
        sg.Combo(
            ["All", "With flags", "Without flags"],
            size=(10, 1),
            k="-FLAGS CHOICE-",
            enable_events=True,
            readonly=True,
            default_value="All",
            font=config.GUI_NORMAL_FONT,
        ),
    ]
    return filter_elems


def draw_figure(canvas, figure):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side="bottom", fill="both", expand=1)
    return figure_canvas_agg


def draw_chart(data, figure_vars, bar_width=0.15):
    # handle the case where the figure has not yet been initialized
    try:
        figure_vars["fig_agg"].get_tk_widget().forget()
        plt.clf()
    except AttributeError:
        figure_vars["plt_fig"] = plt.figure()
    _, active_no_flags, active_w_flags, inactive, total = zip(*data)
    xlabels = [str(i) for i in range(1, 11)] + ["Total"]

    x = np.arange(len(xlabels))

    mc = mcolors.TABLEAU_COLORS

    d1 = plt.bar(
        x - 1.5 * bar_width,
        active_no_flags,
        bar_width,
        label="Active without flags",
        color=mc["tab:red"],
    )
    d2 = plt.bar(
        x - 0.5 * bar_width,
        active_w_flags,
        bar_width,
        label="Active with flags",
        color=mc["tab:orange"],
    )
    d3 = plt.bar(
        x + 0.5 * bar_width, inactive, bar_width, label="Inactive", color=mc["tab:blue"]
    )
    d4 = plt.bar(
        x + 1.5 * bar_width, total, bar_width, label="Total", color=mc["tab:gray"]
    )

    # Add some text for labels, title, and custom x-axis tick labels, etc.
    plt.ylabel("Number of assays")
    plt.xlabel("Key characteristic")
    plt.xticks(x, labels=xlabels)
    plt.legend(loc="upper left")
    if plot_annotation_text:
        xmin, xmax, ymin, ymax = plt.axis()
        xloc = (xmin + xmax) / 3
        yloc = 0.9 * ymax
        plt.text(
            xloc,
            yloc,
            plot_annotation_text,
            ha="center",
            va="center",
            size=6,
            bbox=dict(boxstyle="round", pad=0.4, fc="cornsilk"),
        )
    plt.tight_layout()
    figure_vars["fig_agg"] = draw_figure(
        figure_vars["window"]["-FIGURE CANVAS-"].TKCanvas, figure_vars["plt_fig"]
    )


def get_kc_summary_data(sdat: dict) -> pd.DataFrame:
    """The data structure below is tightly coupled to the `summarize_kc_data`
    method in the `analyze` module
    """
    inds = list(config.KC_NUM_TO_NAME.values()) + ["Total"]
    data = [
        [
            sdat[v]
            for v in [
                "kc1_active_no_flags",
                "kc1_active_w_flags",
                "kc1_inactive",
                "kc1_total",
            ]
        ],
        [
            sdat[v]
            for v in [
                "kc2_active_no_flags",
                "kc2_active_w_flags",
                "kc2_inactive",
                "kc2_total",
            ]
        ],
        [
            sdat[v]
            for v in [
                "kc3_active_no_flags",
                "kc3_active_w_flags",
                "kc3_inactive",
                "kc3_total",
            ]
        ],
        [
            sdat[v]
            for v in [
                "kc4_active_no_flags",
                "kc4_active_w_flags",
                "kc4_inactive",
                "kc4_total",
            ]
        ],
        [
            sdat[v]
            for v in [
                "kc5_active_no_flags",
                "kc5_active_w_flags",
                "kc5_inactive",
                "kc5_total",
            ]
        ],
        [
            sdat[v]
            for v in [
                "kc6_active_no_flags",
                "kc6_active_w_flags",
                "kc6_inactive",
                "kc6_total",
            ]
        ],
        [
            sdat[v]
            for v in [
                "kc7_active_no_flags",
                "kc7_active_w_flags",
                "kc7_inactive",
                "kc7_total",
            ]
        ],
        [
            sdat[v]
            for v in [
                "kc8_active_no_flags",
                "kc8_active_w_flags",
                "kc8_inactive",
                "kc8_total",
            ]
        ],
        [
            sdat[v]
            for v in [
                "kc9_active_no_flags",
                "kc9_active_w_flags",
                "kc9_inactive",
                "kc9_total",
            ]
        ],
        [
            sdat[v]
            for v in [
                "kc10_active_no_flags",
                "kc10_active_w_flags",
                "kc10_inactive",
                "kc10_total",
            ]
        ],
    ]
    df = pd.DataFrame(data)
    df.loc["Total"] = df.sum(numeric_only=True, axis=0)
    df.insert(0, "KCS", inds)
    return df


# -------------------------------------------------------------------------
# Create the window layout
# -------------------------------------------------------------------------
def make_window(anl):
    """Create the main window"""

    sg.theme(config.GUI_THEME)

    # Create an unitial table structure
    # This will be replaced when a chemical is selected
    details_placeholder = np.random.random((10, len(details_table_headers)))
    summary_placeholder = np.random.random((10, len(summary_table_headers)))

    kc_details_table = create_kc_table(
        details_placeholder, headers=details_table_headers, key="-KC DETAILS TABLE-"
    )
    kc_summary_table = create_kc_table(
        summary_placeholder, headers=summary_table_headers, key="-KC SUMMARY TABLE-"
    )

    chem_listbox = create_chem_listbox(anl)
    chem_listbox_controls = create_chem_listbox_controls()
    filter_choice_elems = create_filter_choice_elements()
    plot_canvas = create_plot_canvas()

    # ------ Tab and Tab Group definition ------

    tab1_layout = [
        [kc_summary_table],
        [plot_canvas],
    ]

    tab2_layout = [
        [*filter_choice_elems],
        [kc_details_table],
    ]

    tab_group_layout = [
        [
            sg.Tab(
                "Summary",
                tab1_layout,
                visible=True,
                font=config.GUI_NORMAL_FONT,
                key="-SUMMARY TAB-",
            ),
            sg.Tab(
                "Details",
                tab2_layout,
                visible=True,
                font=config.GUI_NORMAL_FONT,
                key="-DETAILS TAB-",
            ),
        ]
    ]

    # ------ Column layouts ------

    left_col = sg.Column(
        [[chem_listbox], [*chem_listbox_controls]],
        element_justification="l",
        expand_x=True,
        expand_y=True,
    )

    right_col = sg.Column(
        [
            [
                sg.Text(
                    "<Chemical name>",
                    font=config.GUI_SUBTITLE_FONT,
                    k="-CHEMICAL NAME-",
                )
            ],
            [
                sg.TabGroup(
                    tab_group_layout,
                    enable_events=True,
                    visible=False,
                    key="-TAB GROUP-",
                )
            ],
        ],
        element_justification="c",
        expand_x=True,
        expand_y=True,
        visible=False,
        k="-RIGHT COL-",
    )

    # ------ Menu definition ------

    menu_def = [
        ["&File", ["!&Save report...", "E&xit"]],
        ["&Help", ["&Instructions...", "&About..."]],
    ]

    # ----- Full layout -----

    full_layout = [
        [sg.Menu(menu_def, size=(50, 50), k="-MENU-")],
        [
            sg.Image(str(config.TITLE_IMAGE), key="-TITLE IMAGE-"),
            sg.Text(config.APP_NAME, font=config.GUI_TITLE_FONT),
        ],
        [
            sg.Pane(
                [left_col, right_col],
                orientation="h",
                relief=sg.RELIEF_SUNKEN,
                k="-PANE-",
            )
        ],
    ]

    # -------------------------------------------------------------------------
    # Instantiate the window
    # -------------------------------------------------------------------------
    window = sg.Window(
        config.APP_NAME,
        full_layout,
        finalize=True,
        icon=icon,
        resizable=True,
        use_default_focus=False,
    )

    window.set_min_size(window.size)

    window["-CHEMICALS LIST-"].expand(True, True, True)
    window["-KC SUMMARY TABLE-"].expand(True, True, True)
    window["-KC DETAILS TABLE-"].expand(True, True, True)
    window["-PANE-"].expand(True, True, True)
    window["-FIGURE CANVAS-"].expand(True, True, True)

    # Set some keyboard shortcuts
    window.bind("<F1>", "-FOCUS FILTER-")
    window.bind("<F2>", "-FOCUS CHEMICALS LIST-")
    window.bind("<F3>", "-FOCUS SUMMARY TAB-")
    window.bind("<F4>", "-FOCUS DETAILS TAB-")
    window.bind("<Control-s>", "-SAVE-")
    window.bind("<Control-S>", "-SAVE-")

    window.bring_to_front()
    window.Maximize()

    return window


def create_about_window():
    sg.popup(
        config.APP_NAME,
        f"Developer: {config.APP_ORG}",
        f"Version: {config.APP_VERSION}",
        f"High throughput screening data source(s): {config.HTS_DATA_SOURCE}",
        f"Key Characteristics of Carcinogens mapping source: {config.KC_MAPPING_SOURCE}",
        title="About",
        grab_anywhere=True,
    )


def create_instructions_window():
    instructions_file = config.DOCS_DIR / "instructions.txt"
    with open(instructions_file, "r") as fh:
        instructions_text = fh.read()
    itext = instructions_text.split("\n")
    l_width = max([len(t) for t in itext])

    sg.popup(
        instructions_text, title="Instructions", grab_anywhere=True, line_width=l_width
    )


# -------------------------------------------------------------------------
# Main program
# -------------------------------------------------------------------------


def main():
    """The main program that instantiates the windows and starts the event loop."""
    cas_number = None

    anl = init_db()
    chemicals_dict = get_chemicals_dict(anl)
    chemicals_list = get_chemicals_list(anl)

    details_table_headers_map = {v: k for k, v in enumerate(details_table_headers)}
    summary_table_headers_map = {v: k for k, v in enumerate(summary_table_headers)}

    window = make_window(anl)
    window["-NUMBER REMAINING-"].update(f"{len(chemicals_list)} chemicals")
    window.force_focus()

    figure_vars = {"window": window, "fig_agg": False, "plt_fig": False}

    # The default table justification is "center". Certain columns look better
    # left justified. PySimpleGUI doesn't make this easy, so we drop down to Tk.
    cols_to_leftjustify = ["Key characteristic", "Assay name", "Flags"]
    for c in cols_to_leftjustify:
        cnum = details_table_headers_map[c]
        window["-KC DETAILS TABLE-"].Widget.column(cnum, anchor="w")
    cols_to_leftjustify = ["Key characteristic"]
    for c in cols_to_leftjustify:
        cnum = summary_table_headers_map[c]
        window["-KC SUMMARY TABLE-"].Widget.column(c, anchor="w")

    # -------------------------------------------------------------------------
    # The event loop
    # -------------------------------------------------------------------------
    while True:
        event, values = window.read()

        if event in (sg.WINDOW_CLOSED, "-EXIT-"):
            break

        if event == "Exit":
            break

        if event == "Save report...":
            event = "-SAVE-"

        if event == "About...":
            create_about_window()

        if event == "Instructions...":
            create_instructions_window()

        if (
            event == "-CHEMICALS LIST-"
        ):  # if double clicked (used the bind return key param)
            event = "-PROCESS-"

        if event == "-PROCESS-":
            chem_choice = values["-CHEMICALS LIST-"]
            if chem_choice:
                window["-RIGHT COL-"].update(visible=True)

                # get the value in the list that was selected
                # and find the corresponding CAS number
                ident = values["-CHEMICALS LIST-"][0]
                cas_number = chemicals_dict[ident]

                # update the chemical name that is shown
                window.find_element("-CHEMICAL NAME-").Update(value=ident)

                # update the details table
                details, _ = anl.clean_kc_data_for_presentation(cas_number)
                details_data = details.values.tolist()
                window.find_element("-KC DETAILS TABLE-").Update(values=details_data)

                # update the summary table
                sdata = anl.summarize_kc_data(cas_number)
                summary = get_kc_summary_data(sdata)
                summary_data = summary.values.tolist()
                window.find_element("-KC SUMMARY TABLE-").Update(values=summary_data)

                # return filters to default values
                window.find_element("-KEYCHAR CHOICE-").Update(value="All")
                window.find_element("-ACTIVITY CHOICE-").Update(value="All")
                window.find_element("-FLAGS CHOICE-").Update(value="All")

                # change the menu since we now have an active selection
                menu_def = [
                    ["&File", ["&Save report...", "E&xit"]],
                    ["&Help", ["&Instructions...", "&About..."]],
                ]

                window.find_element("-MENU-").Update(menu_definition=menu_def)
                draw_chart(summary_data, figure_vars)

        elif (
            (event == "-KEYCHAR CHOICE-")
            or (event == "-ACTIVITY CHOICE-")
            or (event == "-FLAGS CHOICE-")
        ):
            # One of the results filters has been selected

            # We use the sentinel None to indicate that no filtering should be done

            # filter on key characteristics
            if values["-KEYCHAR CHOICE-"] == "All":
                keychar_num = None
            else:
                keychar_num = config.KC_NAME_TO_NUM[values["-KEYCHAR CHOICE-"]]

            # filter on activity
            if values["-ACTIVITY CHOICE-"] == "Active":
                active = True
            elif values["-ACTIVITY CHOICE-"] == "Inactive":
                active = False
            else:
                active = None

            # filter on flags
            if values["-FLAGS CHOICE-"] == "With flags":
                has_flags = True
            elif values["-FLAGS CHOICE-"] == "Without flags":
                has_flags = False
            else:
                has_flags = None

            filtered = filter_details_data(details, keychar_num, active, has_flags)
            filtered_data = filtered.values.tolist()
            window.find_element("-KC DETAILS TABLE-").Update(values=filtered_data)

        elif event == "-CHEMICALS FILTER BOX-":
            new_list = [
                i
                for i in chemicals_list
                if values["-CHEMICALS FILTER BOX-"].lower() in i.lower()
            ]
            window["-CHEMICALS LIST-"].update(new_list)
            window["-NUMBER REMAINING-"].update(f"{len(new_list)} chemicals")

        elif event == "-FOCUS FILTER-":
            window["-CHEMICALS FILTER BOX-"].set_focus()

        elif event == "-FOCUS CHEMICALS LIST-":
            window["-CHEMICALS LIST-"].set_focus()

        elif event == "-FOCUS DETAILS TAB-":
            window["-DETAILS TAB-"].select()

        elif event == "-FOCUS SUMMARY TAB-":
            window["-SUMMARY TAB-"].select()

        elif event == "-CLEAR-":
            chemicals_list = get_chemicals_list(anl)
            window["-CHEMICALS FILTER BOX-"].update("")
            window["-NUMBER REMAINING-"].update(f"{len(chemicals_list)} chemicals")
            window["-CHEMICALS LIST-"].update(chemicals_list)

        elif event == "-SAVE-":
            if cas_number:
                fname = sg.popup_get_file(
                    title="Save report",
                    message="Select a name and location for the saved report",
                    save_as=True,
                    history=False,
                    file_types=(("Excel", "*.xlsx"),),
                )
                try:
                    if fname:
                        write.save_kc_data_to_excel(anl, cas_number, save_to=fname)
                except Exception as e:
                    sg.popup_error(
                        "There was a problem saving your report.\nMake sure that you have selected a valid name and location.",
                        f"The full error message is as follows: {e}",
                        title="Error",
                    )

    window.close()


# -------------------------------------------------------------------------

if __name__ == "__main__":
    with open(config.ICON_IMAGE, "rb") as fh:
        icon = fh.read()

    main()
