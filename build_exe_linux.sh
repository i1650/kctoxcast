#!/bin/sh

VERSION="0.7.7"
PRODUCT="kc-hits"
FILE_NAME="$PRODUCT"_v"$VERSION"
EXE_NAME="$FILE_NAME".exe
MAIN_PY_FILE="./src/kc_hits.py"
DB_DIR="./src/db"
DATA_DIR="./src/data"
TEMPLATES_DIR="./src/templates"
DOCS_DIR="./src/docs"
RESOURCES_DIR="./src/resources"
NUITKA_DIST_DIR="./dist/nuitka"
PYINSTALLER_DIST_DIR="./dist/pyinstaller"
ICON_FILE="./src/resources/aflatoxin.png"

PYINSTALLER="/Users/breisfel/anaconda3/envs/kc_hits/bin/pyinstaller"
PYTHON="/Users/breisfel/anaconda3/envs/kc_hits/bin/python"

# build standalone exe
echo
echo "--------------------------------------------"
echo "Building standalone exe using PyInstaller..."
echo "--------------------------------------------"
$PYINSTALLER --distpath="$PYINSTALLER_DIST_DIR" --clean kc_hits.spec

# build wheel
echo
echo "------------------------"
echo "Building python wheel..."
echo "------------------------"
$PYTHON setup.py bdist_wheel

# build a 'compiled' executable using Nuitka
echo
echo "---------------------------------------"
echo "Building standalone exe using Nuitka..."
echo "---------------------------------------"
$PYTHON -m nuitka \
    --onefile \
    --disable-console \
    --follow-imports \
    --enable-plugin="tk-inter" \
    --include-data-dir="$DB_DIR"=db \
    --include-data-dir="$DOCS_DIR"=docs \
    --include-data-dir="$TEMPLATES_DIR"=templates \
    --include-data-dir="$RESOURCES_DIR"=resources \
    --include-data-dir="$DATA_DIR"=data \
    --output-filename="$FILE_NAME" \
    "$MAIN_PY_FILE"

# move Nuitka executable to the dists area
if [ ! -d "$NUITKA_DIST_DIR" ]; then
    mkdir "$NUITKA_DIST_DIR"
fi
mv $EXE_NAME $NUITKA_DIST_DIR
