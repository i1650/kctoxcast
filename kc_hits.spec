# -*- mode: python ; coding: utf-8 -*-

import sys
import os

ojoin = os.path.join

proj_root = os.getcwd()
proj_dir = ojoin(proj_root, "src")

db_dir = ojoin(proj_dir, "db")
data_dir = ojoin(proj_dir, "data")

sys.path.append(proj_dir)
from config import APP_VERSION

block_cipher = None

added_files = [
    (ojoin(data_dir, "keychar_toxcast_mapping_20211119.csv"), "data"),
    (ojoin(db_dir  , "assay_info_20230929.pkl"), "db"),
    (ojoin(db_dir  , "toxcast_data_20230929.pkl"), "db"),
    (ojoin(proj_dir, "templates/toxcast_data_template_20211119.xlsx"), "templates"),
    (ojoin(proj_dir, "resources/aflatoxin.png"), "resources"),
    (ojoin(proj_dir, "resources/kcs.png"), "resources"),
    (ojoin(proj_dir, "docs/instructions.txt"), "docs"),
]

a = Analysis(
    [ojoin(proj_dir, "kc_hits.py")],
    pathex=[proj_dir],
    binaries=[],
    datas=added_files,
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)

pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    [],
    name=f"kc-hits_v{APP_VERSION}",
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=False,
    disable_windowed_traceback=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
