@echo off

set VERSION=0.7.7
set COMPANY=IARC Monographs
set PRODUCT=kc-hits
set FILE_NAME="%PRODUCT%"_v"%VERSION%"
set EXE_NAME="%FILE_NAME%".exe
set MAIN_PY_FILE=.\src\kc_hits.py
set DB_DIR=.\src\db
set DATA_DIR=.\src\data
set TEMPLATES_DIR=.\src\templates
set DOCS_DIR=.\src\docs
set RESOURCES_DIR=.\src\resources
set NUITKA_DIST_DIR=.\dist\nuitka
set PYINSTALLER_DIST_DIR=.\dist\pyinstaller
set ICON_FILE=.\src\resources\aflatoxin.png
set SPLASH_IMAGE=.\images\kc-hits_splash.png

call activate kc_hits

rem build standalone exe
echo:
echo --------------------------------------------
echo Building standalone exe using PyInstaller...
echo --------------------------------------------
pyinstaller --distpath="%PYINSTALLER_DIST_DIR%" --clean kc_hits.spec

rem build wheel
echo:
echo ------------------------
echo Building python wheel...
echo ------------------------
python setup.py bdist_wheel

rem build a 'compiled' executable using Nuitka
echo:
echo ---------------------------------------
echo Building standalone exe using Nuitka...
echo ---------------------------------------
python -m nuitka ^
    --onefile ^
    --disable-console ^
    --follow-imports ^
    --enable-plugin=tk-inter ^
    --include-data-dir="%DB_DIR%"=db ^
    --include-data-dir="%DOCS_DIR%"=docs ^
    --include-data-dir="%TEMPLATES_DIR%"=templates ^
    --include-data-dir="%RESOURCES_DIR%"=resources ^
    --include-data-dir="%DATA_DIR%"=data ^
    --product-name="%PRODUCT%" ^
    --file-version="%VERSION%" ^
    --company-name="%COMPANY%" ^
    --output-filename="%FILE_NAME%" ^
    --windows-icon-from-ico="%ICON_FILE%" ^
	--onefile-windows-splash-screen-image="%SPLASH_IMAGE%" ^
    "%MAIN_PY_FILE%"

rem move Nuitka executable to the dists area
if not exist "%NUITKA_DIST_DIR%" mkdir "%NUITKA_DIST_DIR%"
move "%EXE_NAME%" "%NUITKA_DIST_DIR%"

call conda deactivate
