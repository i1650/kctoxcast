#!/bin/sh

VERSION="0.7.7"
PRODUCT="kc-hits"
AUTHOR="IARC Monographs Programme"
FILE_NAME="$PRODUCT"_v"$VERSION"
EXE_NAME="$FILE_NAME".app
MAIN_PY_FILE="./src/kc_hits.py"
DB_DIR="./src/db"
DATA_DIR="./src/data"
TEMPLATES_DIR="./src/templates"
DOCS_DIR="./src/docs"
RESOURCES_DIR="./src/resources"
NUITKA_DIST_DIR="./dist/nuitka"
PY2APP_DIST_DIR="./dist/py2app"
ICON_FILE="./src/resources/aflatoxin.png"

PY2APP="/Users/breisfel/anaconda3/envs/kc_hits/bin/py2app"
PYTHON="/Users/breisfel/anaconda3/envs/kc_hits/bin/python"

# build standalone exe
#echo
#echo "---------------------------------------"
#echo "Building standalone exe using py2app..."
#echo "---------------------------------------"
#$PY2APP --distpath="$PYINSTALLER_DIST_DIR" --clean kc_hits.spec

# build wheel
echo
echo "------------------------"
echo "Building python wheel..."
echo "------------------------"
$PYTHON setup.py bdist_wheel

# build a 'compiled' executable using Nuitka
echo
echo "---------------------------------------"
echo "Building standalone exe using Nuitka..."
echo "---------------------------------------"
$PYTHON -m nuitka \
    --standalone \
    --disable-console \
    --macos-create-app-bundle \
    --follow-imports \
    --enable-plugin="tk-inter" \
    --macos-app-name="$PRODUCT" \
    --macos-app-version="$VERSION" \
    --include-data-dir="$DB_DIR"=db \
    --include-data-dir="$DOCS_DIR"=docs \
    --include-data-dir="$TEMPLATES_DIR"=templates \
    --include-data-dir="$RESOURCES_DIR"=resources \
    --include-data-dir="$DATA_DIR"=data \
    --macos-app-icon="$ICON_FILE" \
    --output-filename="$FILE_NAME" \
    "$MAIN_PY_FILE"

# move Nuitka executable to the dists area
if [ ! -d "$NUITKA_DIST_DIR" ]; then
    mkdir "$NUITKA_DIST_DIR"
fi
#mv $EXE_NAME $NUITKA_DIST_DIR
